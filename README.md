![Text banner that says "Bloodlust" in uppercase, and "Theme 14 by glenthemes" at the bottom](https://64.media.tumblr.com/12402fc6b521826bd51bfb73b0e21d5a/0923108176c1dedf-45/s1280x1920/7658f8f22a920b69c719973fb5c53c80b7d62ffe.png)  
![Screenshot preview of the theme "Bloodlust" by glenthemes](https://64.media.tumblr.com/24c4bf4e8066a295f1357aaa764d9269/0923108176c1dedf-a7/s1280x1920/02367568710ea07514b5fe9bfc0ce86e88238bbb.gif)

**Theme no.:** 14  
**Theme name:** Bloodlust  
**Theme type:** Free / Tumblr use  
**Description:** <small>BLOODLUST</small> is a Bloodborne-inspired fansite theme with a header and 2 sidebars with multiple places for description and status text, affiliates, and links to other socials; header art by [@snatti](https://snatti.tumblr.com/post/165287077064/just-wanted-to-compile-all-the-fanart-i-did-for).  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-12-13](https://64.media.tumblr.com/f01c0dc81036343f482eec8a6b2db147/tumblr_nzb1ap1Znf1ubolzro1_540.gif)  
**Rework date [v1]:** [2020-03-20](https://64.media.tumblr.com/b6d3fecd56868b56dcb566336087587d/0923108176c1dedf-83/s1280x1920/f84ee744f6e3907e89c37d33f155e76b98b1892d.png)  
**Rework date [v2]:** 2021-11-18

**Post:** [glenthemes.tumblr.com/post/613159366780125184](https://glenthemes.tumblr.com/post/613159366780125184)  
**Preview:** [glenthpvs.tumblr.com/bloodlust](https://glenthpvs.tumblr.com/bloodlust)  
**Download:** [pastebin.com/5d5g0mWT](https://pastebin.com/5d5g0mWT)  
**Guide:** [docs.google.com/presentation/d/19QzgXtEY1JawYEh8vudyXcjFBZT5wgKvDxUCt0bHDqE/edit?usp=sharing](https://docs.google.com/presentation/d/19QzgXtEY1JawYEh8vudyXcjFBZT5wgKvDxUCt0bHDqE/edit?usp=sharing)  
**Credits:** [glencredits.tumblr.com/bloodlust](https://glencredits.tumblr.com/bloodlust)
