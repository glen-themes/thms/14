// WHAT IS THE BEST MUSIC FOR FISHING?
// ANYTHING WITH HEAVY BASS IN IT

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html");
    var boob = bob[0];

    if(customize_page){
        boob.setAttribute("customize-page","true");
    } else if(on_main){
        boob.setAttribute("customize-page","false");
    }

    document.querySelectorAll("span[src-link]").forEach(uvu => {
        var vek = uvu.textContent;

        if(vek.indexOf("-") > -1){
            var lastdab = vek.lastIndexOf("-");
            var manydab = vek.substring(lastdab+1);
            if(manydab.substring(0,4) == "deac"){
                uvu.textContent = vek.substring(0,vek.lastIndexOf("-")) + " (deactivated)";
            }
        }
    });
});

function lalala(){
    // top bar height
    var tb = document.getElementsByClassName("topbar")[0];
    var tbh = tb.clientHeight;

    if(tbh == "0"){
        location.reload(true)
    } else {
        // do stuff once it's reinitialized
        document.documentElement.style.setProperty("--Top-Bar-Height",tbh + "px");
        document.querySelectorAll(".le-header, .mynoots").forEach(fizz => {
            fizz.classList.add("showpls")
        });
    }
}

setTimeout(() => {
    lalala();
},1000);

// fallback for if the content doesn't show

// check if there is an existing winload
// if yes, add to existing and execute them together
// if not, create one and go
// SOURCE:  stackoverflow.com/a/5304806/8144506
function winload(func){
    var oldonload = window.onload;
    if(typeof window.onload != "function"){
        window.onload = func;
    } else {
        window.onload = function(){
            if(oldonload){
                oldonload();
            }

            func();
        }
    }
}

winload(function(){
	lalala();
});

// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// external link icon
// feathericons
var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');

// 'previous page' svg
// feathericons
var prev = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='11 17 6 12 11 7'></polyline><polyline points='18 17 13 12 18 7'></polyline></svg>";

document.documentElement.style.setProperty('--BackSVG','url("' + prev + '")');

// 'next page' svg
// feathericons
var next = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='13 17 18 12 13 7'></polyline><polyline points='6 17 11 12 6 7'></polyline></svg>";

document.documentElement.style.setProperty('--NextSVG','url("' + next + '")');

// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");

    $(".tumblr_preview_marker___").remove();

    /*-------- TOOLTIPS --------*/
    $("a[title]:not([title=''])").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:69,
        attribute:"title"
    });

    /*------- REMOVE NPF VIDEO AUTOPLAY -------*/
    $("video[autoplay='autoplay']").each(function(){
        $(this).removeAttr("autoplay")
    });

    /*----------- REMOVE <p> WHITESPACE -----------*/
    $(".postinner p").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }

        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })

    $(".postinner p, .postinner blockquote, .postinner ol, .postinner ul").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom",0)
        }

        if($(this).next().is(".tagsdiv")){
            $(this).css("margin-bottom",0)
        }
    })

    // remove empty captions
    $(".caption").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove()
        }
    })

    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })

    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });

            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);

    $(".albumwrap").click(function(){

        var emp = $(this).parents(".audiowrap").find("audio")[0];

        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }

        var that = this

        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })

    // minimal soundcloud player Ãƒâ€šÃ‚Â© shythemes.tumblr
    var color = getComputedStyle(document.documentElement)
               .getPropertyValue("--Body-Text-Color");

    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });

    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + color.split('#')[1], height: 116, width: '100%' });
       });
    }

    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })

    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        $(this).find("a.tumblr_blog").remove();

        $(this).find("span").each(function(){
            if($.trim($(this).text()) == "(via"){
                $(this).remove();
            }

            if($.trim($(this).text()) == ")"){
                $(this).remove();
            }
        })

        $(this).html(
            $(this).html().replace("(via","")
        )

        $(this).find("span:first").each(function(){
            if(!$(this).next().length){
                $(this).remove()
            }
        })

        $(this).find("p").each(function(){
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }

            if($(this).children("br").length){
                if(!$(this).children().first().siblings().length){
                    $(this).remove()
                }
            }

            $(this).html($.trim($(this).html()));

            if($(this).text() == ")"){
                $(this).remove()
            }
        })

        $(this).find("p:last").each(function(){
            if($(this).find("a[href*='tumblr.com/post']").length){
                $(this).remove()
            }
        })

        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })

    $("[mdash] + p").each(function(){
        if(!$(this).next().length){
            if($.trim($(this).text()) !== ""){
                var sto = " " + $(this).html();
                $(this).prev().append(sto)
                $(this).remove();
            }
        }
    })

    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })

    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })

        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");

        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })

    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row").andSelf().wrapAll('<div class="chatwrap">');
            } else {
                $(this).nextUntil(":not(.chat_row").addBack().wrapAll('<div class="chatwrap">');
            }
        });
    })

    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextUntil(".tagsdiv").wrapAll("<div class='reblog-comment'>")
        }
    })

    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
    })



    /*-------- CLICKTAGS --------*/
    var tags_ms = parseInt(getComputedStyle(document.documentElement)
                   .getPropertyValue("--Tags-Fade-Speed-MS"));


    $(".clicktags").click(function(){
        var that = this;
        var tagsdiv = $(this).parents(".permadiv").prev(".postinner").find(".tagsdiv");

        if(!$(this).hasClass("clique")){
            $(this).addClass("clique");
            tagsdiv.slideDown(tags_ms);
            setTimeout(function(){
                tagsdiv.addClass("tagsfade");
            },tags_ms);
        } else {
            tagsdiv.removeClass("tagsfade");
            setTimeout(function(){
                tagsdiv.slideUp(tags_ms);
                $(that).removeClass("clique");
            },tags_ms)
        }
    })

    /*----------- POST NOTES -----------*/
    $("ol.notes a[title]").each(function(){
        $(this).removeAttr("title")
    });

    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });

    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();

    // make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();

            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;

            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;

            $(this).height(rat_h)
        }
    })

    /*--- fvck tvmblr ---*/
    var imgs = document.querySelectorAll("img");
    Array.prototype.forEach.call(imgs, function(invis){
      if(invis.src.indexOf("assets.tumblr.com/images/x.gif") > -1){
        invis.setAttribute("src","https://cdn.glitch.com/bdf00c8f-434a-46d9-a514-ec8332ec176a/1x1.png");
      }
    });

    /*----- OTHER -----*/
    if(customize_page){
        $(".reblog-head img").each(function(){
            if($(this).attr("src") == $("html[portrait]").attr("portrait")){
                $(this).remove()
            }
        })
    }

    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })

    /*---- LEFT SIDEBAR - OTHER BLOGS ----*/
    $(".blogs-box").each(function(){
        var pengu = $.trim($(this).html());
        pengu = pengu.replaceAll("***","<div class='blog-row'></div>")
                     .replaceAll("[pic]","<img>")
                     .replaceAll("[link-text]","<span class='blog-tit'></span>")
                     .replaceAll("[link-url]","<a href=\'\'></a>")
                     .replaceAll("[subtitle]","<span class='blog-subtitle'></span>");
        $(this).html(pengu);

        $(this).contents().filter(function(){
            return this.nodeType === 3 && this.data.trim().length > 0
        }).wrap("<span>")
    });

    $(".blogs-box span:not([class])").each(function(){
        $(this).html($.trim($(this).html()))
    })

    $(".blog-row").each(function(){
        $(this).nextUntil(".blog-row").prependTo($(this));
        if($.trim($(this).html()) == ""){
            $(this).remove()
        }
    })



    $(".blog-row img").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).attr("src",spt);
                    $(this).remove();
                    $(that).nextAll().wrapAll("<div class='mt'>")
                }

            }
        })

        if($(this).attr("src") == "" || !$(this).is("[src]")){
            $(this).hide();
        }
    })

    // get the blog DISPLAY TEXT
    $(".blog-tit").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).nextAll("a:first").text(spt).addClass("blog-tit");
                    $(that).add($(that).next("span")).remove()
                }
            }
        })
    })

    // target the new a.blog-tit, get the next span
    // which is the LINK URL
    // then apply it to the <a>
    $("a.blog-tit").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).attr("href",spt);
                    $(this).remove()
                }
            }
        });

        var vvb = $.trim($(this).html());
        if(vvb.slice(0,1) == "@"){
            $(this).html(vvb.replace("@","<span et>@</span>"))
        }
    })

    // generate blog subtitle
    $(".blog-subtitle").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).text(spt);
                    $(this).remove()
                }
            }
        });

        if($(this).prev().is(".blog-tit")){
            var vbt = $.trim($(this).prev().text());
            if(vbt.slice(0,1) == "@"){
                $(this).addClass("has-et")
            }
        }
    });

    /*---- LEFT SIDEBAR - SOCIALS BOX ----*/
    $(".socials-box").each(function(){
        var pengu = $.trim($(this).html());
        pengu = pengu.replaceAll("***","<div class='soc-item'></div>")
                     .replaceAll("[icon-name]","<i class='bootstrap-icon'></i>")
                     .replaceAll("[link-url]","<a href=\'\'></a>")
                     .replaceAll("[hover-text]","<span class='soc-hov'></span>");
        $(this).html(pengu);

        $(this).contents().filter(function(){
            return this.nodeType === 3 && this.data.trim().length > 0
        }).wrap("<span>")
    });

    $(".socials-box span:not([class])").each(function(){
        $(this).html($.trim($(this).html()))
    })

    $(".soc-item").each(function(){
        $(this).nextUntil(".soc-item").prependTo($(this));
        if($.trim($(this).html()) == ""){
            $(this).remove()
        }
    })

    // generate social icon
    $(".socials-box .bootstrap-icon").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).addClass("bi-" + spt);
                    $(this).remove()
                }
            }
        });
    })

    // get the hover text
    $(".soc-item .soc-hov").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).parents(".soc-item").find("a:first").attr("title",spt);
                    $(this).add($(that)).remove()
                }
            }
        });
    })

    // apply link to the icon
    $(".soc-item a").each(function(){
        var that = this;
        $(this).next("span").each(function(){
            var t0n = $.trim($(this).text());
            var brascondtn = t0n.indexOf("[") >= 0 && t0n.indexOf("]") >= 0;
            if(brascondtn){
                if(t0n.slice(0,1) == "[" && t0n.slice(-1) == "]"){
                    var spt = t0n.slice(1, -1);
                    $(that).attr("href",spt);
                    $(this).remove()
                }
            }
        });

        // wrap <a> around the icon
        if($(this).prev().is(".bootstrap-icon")){
            $(this).prev().appendTo($(this))
        }
    });

    /*---- RIGHT SIDEBAR BOXES ----*/
    $(".statsbox").each(function(){
        var statext = $.trim($(this).html());
        var statext = statext.replaceAll("* ","<span class='stat-label'></span>");

        $(this).html(statext);

        $(this).contents().filter(function(){
            return this.nodeType === 3 && this.data.trim().length > 0
        }).wrap("<span>")
    })

    $(".stat-label").each(function(){
        $(this).nextUntil(".stat-label").wrapAll("<span class='stat-detail'>");
    })

    $(".stat-detail").each(function(){
        var vert_text = $.trim($(this).text());
        var vert_html = $.trim($(this).html());

        var fsprt = vert_text.split(":")[0];
        $(this).prev(".stat-label").text(fsprt + ":");

        $(this).html(vert_html.replace(fsprt + ":",""))
    })

    $(".stat-label + .stat-detail").each(function(){
        $(this).add($(this).prev()).wrapAll("<div class='stat-grp'>")
    })

    $("[two-cols] .stat-grp:nth-child(even)").each(function(){
        $(this).add($(this).prev(".stat-grp")).wrapAll("<div class='stat-row'>")
    })

    /*---- MSG TO USER ON CUSTOMIZE PAGE ----*/
    if(customize_page){
        $(".msby").html("HELLO! Please read <a href=\'https://docs.google.com/presentation/d/19QzgXtEY1JawYEh8vudyXcjFBZT5wgKvDxUCt0bHDqE/edit?usp=sharing\'>this guide</a> to set up your theme!");
    }

    /*---- CHAD ----*/
    if(!$(".navlinks a[href*='glenthemes.tumblr.com']").length){
        $("body").eq(0).append("<a class='lactose-intolerance' href=\'//glenthemes.tumblr.com\' title=\'&#34;bloodlust&#34; by glenthemes\'>glenthemes</a>");
        $(".lactose-intolerance").css({
            "position":"fixed",
            "bottom":"0",
            "margin-bottom":"12px",
            "right":"0",
            "margin-right":"10px",
            "line-height":"1em",
            "padding":"8px",
            "background":"var(--Top-Bar-BG)",
            "border-radius":"3px",
            "font-family":"var(--SmallCaps-Font-Family)",
            "font-size":"calc(var(--SmallCaps-Font-Size) - 1.5px)",
            "text-transform":"uppercase",
            "letter-spacing":"1px",
            "color":"var(--Top-Bar-Title-Center)",
            "z-index":"69"
        })
    }

    /*--- REMOVE .CUSTOMLINKS IF IT'S EMPTY ---*/
    $(".customlinks").each(function(){
        if($.trim($(this).html()) == ""){
            $(this).remove();
        }
    })

    /*--- UNWRAP THE BLOGTITLE THING (tumblr wtf lol) ---*/
    $(".tumblr_theme_marker_blogtitle").contents().unwrap();
});//end jquery / end ready
